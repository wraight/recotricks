#!/usr/bin/env python2
# coding: utf-8
# info on https://gitlab.cern.ch/wraight/recotricks/tree/master

import ROOT
import re
import argparse
import time

######################
### parsing
######################

def GetArgs():
    
    parser = argparse.ArgumentParser(description=__file__+" arguments")
    
    parser.add_argument('--file', help='gear file to gleen')
    parser.add_argument('--info', help='type of information to return: pos/rot/all')
    parser.add_argument('--output', help='output file name (.txt)')
    
    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'file':"NYS",  'info':"all", 'output':"NYS"}
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################
### get values
######################

def GetValues(filePath, searchStr):

    with open(filePath, 'r') as myfile:
        data=myfile.read()
    #print data

    ### find coordinates that separate planes
    posID=[m.start() for m in re.finditer('ladder ID', data)]
    senID=[m.start() for m in re.finditer('sensitive ID', data)]
    #print posID
    #print senID
    if len(posID)!=len(senID):
        print "problem with lengths: len(posID)=",len(posID),", len(senID)=",len(lenID)

    valArr=[]
    for i in range(0,len(posID)):
        valArr.append( re.search(searchStr, data[posID[i]:senID[i]] ).group(1) )

    return valArr

class fontStyle:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

count=-1
def PrintThis(thisStr, col=-1):
    if col>0:
        print "\x1b["+str(col)+"m"+thisStr+"\x1b[0m"
        return
    
    global count
    if count<0:
        print "\x1b[46m"+thisStr+"\x1b[0m"
    else :
        print "\x1b[47m"+thisStr+"\x1b[0m"
    count*=-1
    return

######################
### main
######################

def main():
    
    argDict=GetArgs()
    print "argDict:",argDict

    totalOutput="NYS"
    
    print fontStyle.BOLD+"plane IDs:"+fontStyle.END
    vals=GetValues(argDict['file'], 'ID=\"(.*)\" positionX')
    PrintThis( ", ".join(["{:0}".format(int(v)) for v in vals]), 42 )
    totalOutput="plane IDs:\n"+", ".join(["{:0}".format(int(v)) for v in vals])
    
    if argDict['info'].lower() in ["all", "pos"]:
        print fontStyle.BOLD+"plane positions:"+fontStyle.END
        totalOutput+="\n\nplane positions:"
        vals=GetValues(argDict['file'], 'positionX=\"(.*)\" positionY')
        PrintThis( "X: "+", ".join(["{:.3f}".format(float(v)) for v in vals]) )
        totalOutput+="\nX: "+", ".join(["{:.3f}".format(float(v)) for v in vals])
        vals=GetValues(argDict['file'], 'positionY=\"(.*)\" positionZ')
        PrintThis( "Y: "+", ".join(["{:.3f}".format(float(v)) for v in vals]) )
        totalOutput+="\nY: "+", ".join(["{:.3f}".format(float(v)) for v in vals])
        vals=GetValues(argDict['file'], 'positionZ=\"(.*)\" rotationXY')
        PrintThis( "Z: "+", ".join(["{:.1f}".format(float(v)) for v in vals]) )
        totalOutput+="\nZ: "+", ".join(["{:.1f}".format(float(v)) for v in vals])
    
    if argDict['info'].lower() in ["all", "rot"]:
        print fontStyle.BOLD+"plane rotations:"+fontStyle.END
        totalOutput+="\n\nplane rotations:"
        vals=GetValues(argDict['file'], 'rotationXY=\"(.*)\" rotationZX')
        PrintThis( "XY: "+", ".join(["{:.1f}".format(float(v)) for v in vals]) )
        totalOutput+="\nXY: "+", ".join(["{:.1f}".format(float(v)) for v in vals])
        vals=GetValues(argDict['file'], 'rotationZX=\"(.*)\" rotationZY')
        PrintThis( "ZX: "+", ".join(["{:.1f}".format(float(v)) for v in vals]) )
        totalOutput+="\nZX: "+", ".join(["{:.1f}".format(float(v)) for v in vals])
        vals=GetValues(argDict['file'], 'rotationZY=\"(.*)\" sizeX')
        PrintThis( "ZY: "+", ".join(["{:.1f}".format(float(v)) for v in vals]) )
        totalOutput+="\nZY: "+", ".join(["{:.1f}".format(float(v)) for v in vals])

    if argDict['output']!="NYS":
        outName=argDict['output']
        if not ".txt" in outName:
            outName+=".txt"
            print ">>> saving",outName
            f = open(outName,'w')
            f.write(totalOutput)
            f.close()

if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()
