###
### get infromation from csv file
###
import csv
import time
import argparse

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)
    
    # pass on
    parser.add_argument('--dois', nargs='+', help='detectors of interest')
    parser.add_argument('--infile', help='csv filename')
    parser.add_argument('--outfile', help='runlist file name (default runlist.csv')
    
    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'dois':[], 'infile':"Downloads/Beamtest-ITK-Desy-December-2018 - Run summary T22.csv", 'outfile':"runlist.csv" }
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict


######################################
### useful functions
######################################


def GetDUTs(fileName, dois):
    
    dutCols=[]
    first=True
    
    input_file = csv.DictReader(open(fileName))
    for row in input_file:
        if first:
            for k in row.keys():
                if "ID DUT" in k: dutCols.append(k)
            #print dutCols
            first=False
        for dc in dutCols:
            if row[dc] not in [d['id'] for d in dois]: dois.append({'id':row[dc],'runs':[],'greens':[]})

    return dutCols


def GetRuns(fileName, dutCols, dois, runStr='run#'):

    for d in dois:
        input_file = csv.DictReader(open(fileName))
        for row in input_file:
            for dc in dutCols:
                if d['id'].lower() in row[dc].lower():
                    d['runs'].append(row[runStr])
                    if "green" in row['Status'].lower():
                        d['greens'].append(row[runStr])


def PrintInfo(fileName, dutCols, max=100, runStr='run#'):
    
    input_file = csv.DictReader(open(fileName))
    count=0
    for row in input_file:
        if count>max: break
        rowStr=row[runStr]+": "
        for dc in dutCols:
            rowStr+=row['ID DUT0']+", "
        if row['Status']=="red":
            rowStr+=" ...RED ROW"
        print rowStr
        count+=1

    print r[runStr]+": "+r['ID DUT0']


def WriteRunlist(colDict, runNums, delim=',', newName="runlist.csv"):

    contents=delim.join(colDict.keys())
    for r in runNums:
        myRow="\n"+delim.join([str(v) for v in colDict.values()])
        contents+=myRow.replace(colDict['RunNumber'],format(int(r), '06'))
    contents+="\n"
    newFile =open(newName, "w")
    newFile.write(contents)
    newFile.close()

    print "wrote:",newName




######################
### main
######################

def main():
    
    argDict=GetArgs()
    print argDict

    fileName=argDict['infile']

    # array of DUT infor dictionaries
    theDois=[]
    
    
    # find column headings for DUT IDs
    theDUTCols=GetDUTs(fileName, theDois)
    print "Found DUTs:\n"+", ".join([d['id'] for d in theDois])

    
    # specify DUTs of interest
    dois=["UK 3292-01-45","UK 3292-01-09"]
    if len(argDict['dois'])>0:
        dois=argDict['dois']
    

    
    # get runs for DUTs of interest (all rows and green rows separately counted)
    GetRuns(fileName,theDUTCols,theDois)
    for doi in dois:
        print doi
        for d in theDois:
            if doi.lower() in d['id'].lower() :
                print d['id']+": "+min(d['runs'])+"-"+max(d['runs'])
                print "runs (greens): "+str(len(d['runs']))+" ("+str(len(d['greens']))+")"
                print d['greens']

    # print out information for N rows
    #PrintInfo(fileName,theDUTCols,20)

    print "Writng CSV file"
    runNums=[]
    for doi in dois:
        for d in theDois:
            if doi.lower() in d['id'].lower() :
                print "adding", len(d['greens'])
                runNums.extend(d['greens'])
    print "with duplicates:",len(runNums)
    runNums=list(dict.fromkeys(runNums))
    print "without duplicates:",len(runNums)
    runNums.sort()
    print "ordered:",len(runNums)
    print runNums
    colDict={'RunNumber':"run#", 'BeamEnergy':5.0, 'GearGeoFile':"gear" }
    WriteRunlist(colDict, runNums, ",", argDict['outfile'])
    print "with runs:\n",runNums


if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"
