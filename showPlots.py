#!/usr/bin/env python2
# coding: utf-8
# info on https://gitlab.cern.ch/wraight/recotricks/tree/master

import ROOT
import argparse
import time

######################
### parsing
######################

def CheckPreSets(hn):
    if "con1" in hn: return "NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ"
    if "con2" in hn: return "NoisyPixelMaskerDUT/detector_XYZ/FiringFreqNoisyPixelDep_dXYZ_HIST"
    if "con3" in hn: return "NoisyPixelMaskerDUT/detector_XYZ/Firing2D_dXYZ"
    if "clu:" in hn: return "ClusteringDUT/detector_XYZ/"+hn.split(':')[1]+"_dXYZ"
    if "hit:" in hn: return "PreAligner/plane_XYZ/hit"+hn.split(':')[1]+"Corr_fixed_to_XYZ"
    if "hit2:" in hn: return "Correlator/Hit"+hn.split(':')[1]+"/Hit"+hn.split(':')[1]+"Correlation_d"+hn.split(':')[2]+"_dXYZ"
    if "ali1:" in hn: return "Align/Residual"+hn.split(':')[1]+"_dXYZ"
    if "ali2:" in hn: return "DafFitter/plXYZ_residual"+hn.split(':')[1]
    if "fit1:" in hn: return "DafFitter/plXYZ_residual"+hn.split(':')[1]
    
    return hn

def GetArgs():
    
    parser = argparse.ArgumentParser(description=__file__+" arguments")
    
    parser.add_argument('--file', help='reco. output file to gleen')
    parser.add_argument('--planes', nargs='+', help='planes to check')
    parser.add_argument('--output', help='output file name (.png)')
    parser.add_argument('--hist', help='generic histogram name with DUT id XYZ and chip type DUT')
    
    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'file':"NYS",  'planes':[], 'output':"NYS", 'hist':"NYS"}
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################
### useful functions
######################

def GetHist(rootFile, seedName, p , plot):

    seedName=seedName.replace("XYZ",p)
    found=False
    planeTypes=["","M26","Mimosa","m26","mimosa"]
    if int(p)>10:
        planeTypes=["APIX", "RD53A", "DUT"]
        
    for d in planeTypes:
        
        histName=seedName.replace("DUT",d)
        try:
            rootFile.GetObject(histName,plot)
        except LookupError:
            print "there is no:",histName,"."
        else:
            print "found: ",histName
            found=True
            break

    if found==False:
        print "No histo found with (seed) name:",seedName
        return None,None

    #print "entries:",plot.GetEntries()
    return plot, str(plot.ClassName())


######################
### main
######################

def main():
    
    argDict=GetArgs()
    print "argDict:",argDict
    
    typeFlag="NYS"
    
    #file and canvas
    rootFile=ROOT.TFile(argDict['file'],"READ",argDict['file'])
    telCan = ROOT.TCanvas( 'telCan', 'telescope planes', 800, 600)
    Ntel=sum(int(i) < 6 for i in argDict['planes'])
    telCan.Divide(3,((Ntel-1)/3)+1)
    dutCan = ROOT.TCanvas( 'dutCan', 'DUT planes', 800, 600)
    Ndut=sum(int(i) > 6 for i in argDict['planes'])
    dutCan.Divide(Ndut,1)
    #print "DUTS:",Ndut

    plotArr=[]
    #loop to get plots
    for p,col in zip(argDict['planes'],range(1,1+len(argDict['planes']),1)):
        
        obj=ROOT.TObject()
        #plot=GetHist(rootFile, "NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ", p, plot)
        #plot=GetHist(rootFile, "NoisyPixelMaskerDUT/detector_XYZ/FiringFreqNoisyPixelDep_dXYZ_HIST", p)
        obj,typeFlag=GetHist(rootFile, CheckPreSets(argDict['hist']), p, obj)

        if obj==None:
            print "No plot returned",p
            continue
    
        ## cast to appropriate object
        plot=ROOT.TObject()
        if typeFlag=="TH2D":
            print "set 2D"
            plot=ROOT.TH2D()
            obj.Copy(plot)
        
        if typeFlag=="TH1D":
            print "set 1D"
            plot=ROOT.TH1D()
            obj.Copy(plot)

        plot.SetStats(0)
        plot.SetLineColor(col)
        if col==5: plot.SetLineColor(col+6)
        plotArr.append({'plot':plot, 'p':p, 'max':plot.GetMaximum(), 'entries':plot.GetEntries(), 'mean':plot.GetMean(), 'rms':plot.GetRMS() })
        #print col
        #print plot.GetNbinsX()

    telInd=1
    dutInd=1
    legs=[]
    for p in plotArr:
        leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
        infoStr="#splitline{p_"+p['p']+":"+"{:0}".format(int(p['entries']))+"}"
        infoStr+="{#splitline{#mu:"+"{:0.3f}".format(float(p['mean']))+"}"
        infoStr+="{rms:"+"{:0.3f}".format(float(p['rms']))+"}}"
        leg.AddEntry(p['plot'],infoStr,"l")
        legs.append(leg)
        if int(p['p'])<7:
            telCan.cd(telInd)
            telInd+=1
        else:
            dutCan.cd(dutInd)
            dutInd+=1
        if typeFlag=="TH2D": p['plot'].Draw("colz")
        else: p['plot'].Draw()
        leg.Draw()

    if telInd>1: #check if any histos found
        telCan.Draw()
        raw_input("press return to close")
    if dutInd>1: #check if any histos found
        dutCan.Draw()
        raw_input("press return to close")
    if telInd==1 and dutInd==1:
        print "NO HISTOGRAMS FOUND. CHECK HISTO NAMES!"


    if argDict['output']!="NYS":
        outNameTel=argDict['output']
        outNameDUT=argDict['output']
        if not ".png" in outNameTel:
            outNameTel+="_tel.png"
        else: outNameTel.replace(".png","_tel.png")
        if not ".png" in outNameDUT:
            outNameDUT+="_dut.png"
        else: outNameDUT.replace(".png","_dut.png")
        telCan.SaveAs(outNameTel)
        dutCan.SaveAs(outNameDUT)


if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()


