#!/usr/bin/env python2
# coding: utf-8
# info on https://gitlab.cern.ch/wraight/recotricks/tree/master

import ROOT
import argparse
import time

######################
### parsing
######################

def CheckPreSets(hn):
    if "clu" in hn: return ['ClusteringDUT/detector_XYZ/totalClusterSize_dXYZ', 'ClusteringDUT/detector_XYZ/clusterSignal_dXYZ', 'ClusteringDUT/detector_XYZ/clusterSizeX_dXYZ', 'ClusteringDUT/detector_XYZ/clusterSizeY_dXYZ', 'ClusteringDUT/detector_XYZ/eventMultiplicity_dXYZ']
    if "fit" in hn: return ['DafFitter/plXYZ_residualX', 'DafFitter/plXYZ_residualY']
    
    return [hn]

def GetArgs():
    
    parser = argparse.ArgumentParser(description=__file__+" arguments")
    
    parser.add_argument('--file', help='cluster output file to gleen')
    parser.add_argument('--planes', nargs='+', help='planes to check')
    parser.add_argument('--output', help='output file name (.txt)')
    parser.add_argument('--hist', help='generic histogram name with DUT id XYZ and chip type DUT')
    parser.add_argument('--data', nargs='+', help='mean/rms/entries/all')
    
    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'file':"NYS",  'planes':[], 'output':"NYS", 'data':[]}
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict


######################
### useful functions
######################

def GetHist1D(rootFile, seedName, p , plot):
    
    seedName=seedName.replace("XYZ",p)
    found=False
    planeTypes=["","M26","Mimosa","m26","mimosa"]
    if int(p)>10:
        planeTypes=["APIX", "RD53A", "DUT"]
    
    for d in planeTypes:
        
        histName=seedName.replace("DUT",d)
        try:
            rootFile.GetObject(histName,plot)
        except LookupError:
            print "there is no:",histName,"."
        else:
            print "found: ",histName
            found=True
            break

    if found==False:
        print "No histo found with (seed) name:",seedName
        return None
    
    #print "entries:",plot.GetEntries()
    return plot


count=-1
def PrintThis(thisStr, col=-1):
    if col>0:
        print "\x1b["+str(col)+"m"+thisStr+"\x1b[0m"
        return
    
    global count
    if count<0:
        print "\x1b[46m"+thisStr+"\x1b[0m"
    else :
        print "\x1b[47m"+thisStr+"\x1b[0m"
    count*=-1
    return

######################
### main
######################

def main():
    
    argDict=GetArgs()
    print "argDict:",argDict
    
    #file and canvas
    rootFile=ROOT.TFile(argDict['file'],"READ",argDict['file'])
    c1 = ROOT.TCanvas( 'can', 'planes', 800,600)
    leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
    c1.Draw()
    
    histInfo=[]
    
    histNames=CheckPreSets(argDict['hist'])

    ## collect plots
    for h in histNames:
        hi={'name':h, 'planes':[], 'means':[], 'RMSs':[], 'entries':[]}
        #loop to get plots
        for p in argDict['planes']:
            plot=ROOT.TH1D()
            plot=GetHist1D(rootFile, h, p, plot)
            if plot==None:
                print "No plot returned",p
                continue
            hi['planes'].append(p)
            hi['means'].append(plot.GetMean())
            hi['RMSs'].append(plot.GetRMS())
            hi['entries'].append(plot.GetEntries())
        histInfo.append(hi)


    totalOutput="NYS"
    
    histData=argDict['data']
    if argDict['data']==["all"]:
        histData=["mean","rms","entries"]
    
    ## print stats
    PrintThis("plane (data) | "+" | ".join(histNames),42)
    totalOutput="plane | "+" | ".join(histNames)
    print "=========================================="
    for p in argDict['planes']:
        outStr=p+" ("+",".join(histData)+") |"
        for h in histNames:
            for hi in histInfo:
                if hi['name']==h:
                    try:
                        ind=hi['planes'].index(p)
                    except ValueError:
                        print "plane",p,"not in list. skip!"
                        outStr+=" --"
                        continue
                    for d in histData:
                        if d.lower() in ["mean", "all"]:
                            outStr+=" {:0.3f}".format(hi['means'][ind])
                        if d.lower() in ["rms", "all"]:
                            outStr+=" {:0.3f}".format(hi['RMSs'][ind])
                        if d.lower() in ["entries", "all"]:
                            outStr+=" {:0}".format(hi['entries'][ind])
                        if d!=histData[-1]: outStr+=", "
                    outStr+=" |"
        PrintThis(outStr)
        totalOutput+="\n"+outStr

    if argDict['output']!="NYS":
        outName=argDict['output']
        if not ".txt" in outName:
            outName+=".txt"
        print ">>> saving",outName
        f = open(outName,'w')
        f.write(totalOutput)
        f.close()


if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()
