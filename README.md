# recoTricks

### Some (python2) code to help debug reconstruction

**NB** Plots are for debugging purposes __not__ for publications (set expectations appropriately). 

Required python libraries:
 * argparse
 * ROOT
 * re

---

## Gadgets to gleen information

## Run Numbers
Get list of run numbers which include DUT of interest from runsheet

**Command**
`runlister.py`

| Args | Comment (default) | e.g. | 
| --- | --- | --- | 
| infile | csv file to scrape | "Beamtest-ITK-Desy-December-2018 - Run summary T22.csv" |
| dois | DUT names of interest (multiple possible) | "UK 3292-01-09" 3292-01-01 | 
| outfile | runlist file name (default runlist.csv) | runlist.csv 

*E.g.* 
`python runlister.py --file "Beamtest-ITK-Desy-December-2018 - Run summary T22.csv" --dois "UK 3292-01-09" 3292-01-09 --outfile runlist.csv `

**_Comments_**
Go to testbeam run sheet and download as csv: File>Download as>Comma-seperated values
***NB*** if you use a file name or DUT name including spaces then use quotations around the name

## Gear information
Gleen parameters from gear file for quick comparison

**Command** 
`getGearParams.py` 

| Args | Comment (default) | e.g. | 
| --- | --- | --- | 
| file | gear file: form the moment only use standard formatted gear files (not set by default) | gear_pre.xml | 
| info | option to specify data: position, rotation, all  (default all) | pos/rot/all | 
| output | output file name (.txt) | myTxt | 

*E.g.* 
`python getGearParams.py --file $EUTELESCOPE/jobsub/examples/datura-noDUT/gear000097_pre.xml --output myTxT`

## Get plot parameters
Get histo mean and RMS from histograms

**Command** 
`getParameterStats.py` 

| Args | Comment (default) | e.g. | 
| --- | --- | --- | 
| file | cluster output file to gleen (not set by default) | runXXXXXX-clustering-histo.root |
| planes | planes to check (none by default) | 1 2 3 21 |
| output | output file name (.txt) | myTxt | 
| hist | generic histogram name | NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ | 
| data | what plot data: mean/rms/entries/all | mean rms entries (= all) | 


*E.g.*
`python getParameterStats.py --file $EUTELESCOPE/jobsub/examples/datura-noDUT/output/histograms/run000097-clustering-histo.root --hist clu --data all --planes 0 1 2 3 4 5 --output myTxT`

**_Comments_**

Default values for _hist_ argument are used to abbreviate common step histograms:

| step | abbr | generic | e.g. | 
| --- | --- | --- | --- |
| clustering | clu | clusterSignal,  clusterSizeX,  clusterSizeY, totalClusterSize, eventMultiplicity | clu |
| fitter | fit | residualX, residualY | fit |

## Get sensor IDs 
Get sensor IDs from converter slcio output.

**Command** 
`findIDs.py` 

| Args | Comment (default) | e.g. | 
| --- | --- | --- | 
| file | converter slcio (not set by default) | runXXXXXX-converter.slcio |
| Nevents | number of events to look through fro IDs (100 by default) | 100 |

*E.g.*
`python findIDs.py --file $EUTELESCOPE/jobsub/examples/datura-noDUT/output/lcio/run000097-converter.slcio --events 1000`

**_Comments_**

**NB** requires `dumpevent` execution

---

## Gadgets for plotting

## Overlay plane plots
Overlay equivalent histos from various planes

**Command** 
`overlayPlanes.py` 

| Args | Comment (default) | e.g. | 
| --- | --- | --- | 
| file | reconstruction step output file to gleen (not set by default) | runXXXXXX-converter-histo.root |
| planes | planes to check (none by default) | 1 2 3 21 |
| output | output file name (.png) | myPlots | 
| hist | generic histogram name with DUT id as XYZ and chip type as DUT | NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ |

*E.g.*
`python overlayPlanes.py --file $EUTELESCOPE/jobsub/examples/datura-noDUT/output/histograms/run000097-converter-histo.root --planes 0 1 2 3 4 5 --output myPlots --hist NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ`

**_Comments_**

Default values for _hist_ argument are used to abbreviate common step histograms:

| step | abbr | generic | e.g. | 
| --- | --- | --- | --- |
| converter | con1 | NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ | con1 |
| converter | con2 | NoisyPixelMaskerDUT/detector_XYZ/FiringFreqNoisyPixelDep_dXYZ_HIST | con2 |
| clustering | clu:**_X/Y_** | ClusteringDUT/detector_XYZ/**_X/Y_**_dXYZ | clu:X |
| hitmaker | hit:**_X/Y_** | PreAligner/plane_XYZ/hit **_X/Y_** Corr_fixed_to_XYZ | hit:X |
| align | ali1:**_X/Y_** | Align/Residual **_X/Y_**_dXYZ | ali1:X |
| align | ali2:**_X/Y_** | DafFitter/plXYZ_residual **_X/Y_** | ali2:X |
| fitter | fit1:**_X/Y_** | DafFitter/plXYZ_residual **_X/Y_** | fit1:X |


## Side by side plane plots
Show equivalent histos from various planes side by side (one canvas for telescope, one for DUTs)

**Command** 
`showPlots.py` 

| Args | Comment (default) | e.g. | 
| --- | --- | --- | 
| file | reconstruction step output file to gleen (not set by default) | runXXXXXX-converter-histo.root |
| planes | planes to check (none by default) | 1 2 3 21 |
| output | output file name (.png) | myPlots | 

*E.g.*
`python showPlots.py --file $EUTELESCOPE/jobsub/examples/datura-noDUT/output/histograms/run000097-converter-histo.root --planes 0 1 2 3 4 5 --output myPlots --hist con3`

**_Comments_**

Default values for _hist_ argument can be used as in _ovelayPlanes.py_ case.
Additional options for 2D histograms:

| step | abbr | generic | e.g. | 
| --- | --- | --- | --- | 
| converter | con3 | NoisyPixelMaskerDUT/detector_XYZ/Firing2D_dXYZ | con3 |
| hitmaker | hit2:**_X/Y_**:**_fixId_** | Correlator/Hit **_X/Y_**/Hit **_X/Y_** Correlation_d **_fixId_** _dXYZ | hit:X:1 |


