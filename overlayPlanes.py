#!/usr/bin/env python2
# coding: utf-8
# info on https://gitlab.cern.ch/wraight/recotricks/tree/master

import ROOT
import argparse
import time

######################
### parsing
######################

def GetArgs():
    
    parser = argparse.ArgumentParser(description=__file__+" arguments")
    
    parser.add_argument('--file', help='reco. output file to gleen')
    parser.add_argument('--planes', nargs='+', help='planes to check')
    parser.add_argument('--output', help='output file name (.png)')
    parser.add_argument('--hist', help='generic histogram name with DUT id XYZ and chip type DUT')
    
    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'file':"NYS",  'planes':[], 'output':"NYS", 'hist':"NYS"}
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################
### useful functions
######################

def CheckPreSets(hn):
    if "con1" in hn: return "NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ"
    if "con2" in hn: return "NoisyPixelMaskerDUT/detector_XYZ/FiringFreqNoisyPixelDep_dXYZ_HIST"
    if "clu:" in hn: return "ClusteringDUT/detector_XYZ/"+hn.split(':')[1]+"_dXYZ"
    if "hit:" in hn: return "PreAligner/plane_XYZ/hit"+hn.split(':')[1]+"Corr_fixed_to_XYZ"
    #if "hit2:" in hn: return "Correlator/Hit"+hn.split(':')[1]+"/Hit"+hn.split(':')[1]+"Correlation_d"+hn.split(':')[2]+"_dXYZ"
    if "ali1:" in hn: return "Align/Residual"+hn.split(':')[1]+"_dXYZ"
    if "ali2:" in hn: return "DafFitter/plXYZ_residual"+hn.split(':')[1]
    if "fit1:" in hn: return "DafFitter/plXYZ_residual"+hn.split(':')[1]

    return hn

def GetHist(rootFile, seedName, p , plot):

    seedName=seedName.replace("XYZ",p)
    found=False
    planeTypes=["","M26","Mimosa","m26","mimosa"]
    if int(p)>10:
        planeTypes=["APIX", "RD53A", "DUT"]
        
    for d in planeTypes:
        
        histName=seedName.replace("DUT",d)
        try:
            rootFile.GetObject(histName,plot)
        except LookupError:
            print "there is no:",histName,"."
        else:
            print "found: ",histName
            found=True
            break

    if found==False:
        print "No histo found with (seed) name:",seedName
        return None

    #print "entries:",plot.GetEntries()
    return plot


######################
### main
######################

def main():
    
    argDict=GetArgs()
    print "argDict:",argDict
    
    #file and canvas
    rootFile=ROOT.TFile(argDict['file'],"READ",argDict['file'])
    c1 = ROOT.TCanvas( 'can', 'planes', 800,600)
    leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
    c1.Draw()

    plotArr=[]
    #loop to get plots
    for p,col in zip(argDict['planes'],range(1,1+len(argDict['planes']),1)):
        plot=ROOT.TH1D()
        #plot=GetHist(rootFile, "NoisyPixelMaskerDUT/detector_XYZ/Firing1D_dXYZ", p, plot)
        #plot=GetHist(rootFile, "NoisyPixelMaskerDUT/detector_XYZ/FiringFreqNoisyPixelDep_dXYZ_HIST", p, plot)
        plot=GetHist(rootFile, CheckPreSets(argDict['hist']), p, plot)
        if plot==None:
            print "No plot returned",p
            continue
        plot.SetStats(0)
        plot.SetLineColor(col)
        if col==5: plot.SetLineColor(col+6)
        plotArr.append({'plot':plot, 'p':p, 'max':plot.GetMaximum()})
        infoStr="p_"+p+":"+str(plot.GetEntries())
        leg.AddEntry(plot,infoStr,"l")
        #print col
        #print plot.GetNbinsX()
    
    maxInd= [p['max'] for p in plotArr].index(max([p['max'] for p in plotArr]))
    #print "maxInd:",maxInd
    plotArr[maxInd]['plot'].Draw()
    for p in plotArr:
        if p['p']==plotArr[maxInd]['p']: continue
        else: p['plot'].Draw("same")

    if len(plotArr)>0: #check if any histos found
        c1.SetLogy()
        c1.Draw()
        leg.Draw()
        #delay close until ready
        raw_input("press return to quit")
    else:
        print "NO HISTOGRAMS FOUND. CHECK HISTO NAMES!"


    if argDict['output']!="NYS":
        outName=argDict['output']
        if not ".png" in outName:
            outName+=".png"
        c1.SaveAs(outName)

if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()


