#!/usr/bin/env python2
# coding: utf-8
# info on https://gitlab.cern.ch/wraight/recotricks/tree/master

import subprocess
import argparse
import time

######################
### parsing
######################

def GetArgs():
    
    parser = argparse.ArgumentParser(description=__file__)
    
    parser.add_argument('--file', help='reco. output file to gleen')
    parser.add_argument('--events', help='planes to check')
    
    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'file':"NYS",  'planes':[], 'events':100}
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict


######################
### main
######################

def main():

    argDict=GetArgs()
    print "argDict:",argDict
    
    Nevents=argDict['events']
    stop=False
    count=1
    sensorIDs=[]
    filledIDs=[]
    while stop==False:

        dumpStr= subprocess.check_output(['dumpevent',argDict['file'],str(count)])
        #print "len:",len(dumpStr)

        if "less than" in dumpStr:
            stop=True
            continue

        ind=0
        while ind>-1:
            ind=dumpStr.find("sensorID:",ind)
            #print "check(",ind,"):",dumpStr[ind-9:ind-3]
            if dumpStr[ind-9:ind-3]=="string": #fudge to remove header
                ind=dumpStr.find("sensorID:",ind+1)
            if ind>-1:
                idStr=dumpStr[ind+9:dumpStr.find(",",ind)]
                nextStr=dumpStr[dumpStr.find("chargeADC",ind):dumpStr.find("chargeADC",ind)+20]
                hits=nextStr.count(",")
                #print "idStr:",idStr
                #print "nextStr:",nextStr
                keep=True
                for i in sensorIDs:
                    if i==idStr:
                        keep=False
                        break
                if keep==True:
                    sensorIDs.append(idStr)
                    if hits>0:
                        filledIDs.append(idStr)
                ind+=10
            else:
                ind=-100

        count+=1
        if count>int(Nevents):
            stop=True
        #print "count({3}):{0}, ind:{1}, ids:{2}, stop:{4}".format(count,ind,sensorIDs,Nevents,stop)

    print "found IDs in",Nevents,"events:",sensorIDs
    print "found FILLED IDs in",Nevents,"events:",filledIDs



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()
